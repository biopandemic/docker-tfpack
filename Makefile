MAINTAINER:=jacoblogemann
IMAGE:=tfpack
TAG:=latest
REPO:=${MAINTAINER}/${IMAGE}

build:
	docker build -t ${REPO}:${TAG} .


docker_run:="docker run -it ${REPO}"
run:
	${docker_run}
run/tf:
	${docker_run} terraform
run/packer:
	${docker_run} packer

push:
	docker push ${REPO}:${TAG}
