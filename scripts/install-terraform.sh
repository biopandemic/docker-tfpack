#!/bin/bash
version=0.6.0
url="https://dl.bintray.com/mitchellh/terraform/terraform_${version}_linux_amd64.zip"
zip_file="/tmp/terraform-${version}.zip"

wget -O ${zip_file} ${url}
unzip -d /bin ${zip_file}
chmod +x /bin/terraform*
rm -f ${zip_file}
