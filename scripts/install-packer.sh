#!/bin/bash
version=0.8.1
url="https://dl.bintray.com/mitchellh/packer/packer_${version}_linux_amd64.zip"
zip_file="/tmp/packer-${version}.zip"

wget -O ${zip_file} ${url}
unzip -d /bin ${zip_file}
chmod +x /bin/packer*
rm -f ${zip_file}
