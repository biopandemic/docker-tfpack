#!/bin/bash
cmd=$1; shift
case $cmd in
     terraform) /bin/terraform $@ ;;
     packer) /bin/packer $@ ;;

     check)
         test -x /bin/terraform && \
         test -x /bin/packer && \
         echo "Check passed." && \
         exit 0
         # If any of that fails, exit w/ error.
         echo "Check failed" && \
         exit -1
         ;;

     *) echo "Only terraform/packer are exposed."
esac
