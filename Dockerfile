FROM gliderlabs/alpine:latest
MAINTAINER Jake Logemann <jake.logemann+docker@gmail.com>


# ------------------------------------------------------------------------
# Install common deps.
# ------------------------------------------------------------------------
RUN apk --update add bash curl ca-certificates && \
    curl -Ls https://circle-artifacts.com/gh/andyshinn/alpine-pkg-glibc/6/artifacts/0/home/ubuntu/alpine-pkg-glibc/packages/x86_64/glibc-2.21-r2.apk > /tmp/glibc-2.21-r2.apk && \
    apk add --allow-untrusted /tmp/glibc-2.21-r2.apk && \
    rm -rf /tmp/glibc-2.21-r2.apk /var/cache/apk/*

# Load install scripts
ADD ./scripts/install-packer.sh /tmp/install-packer.sh
ADD ./scripts/install-terraform.sh /tmp/install-terraform.sh
RUN chmod +x /tmp/install-packer.sh /tmp/install-terraform.sh

# Execute install scripts && clean up.
RUN /tmp/install-packer.sh && /tmp/install-terraform.sh && \
    rm -f /tmp/install-packer.sh /tmp/install-terraform.sh

# Define a default code directory.
VOLUME /code
WORKDIR /code
ONBUILD ADD . /code

# Bind the entrypoint script so only tf/packer can be executed at runtime.
ADD ./scripts/entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]

# By default, print out terraform's help for a quick healthcheck.
CMD ["check"]
